import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit 
{
  @Output() onItemAdded : EventEmitter<DestinoViaje>;
  fg : FormGroup;
  minLong = 5;

  constructor(fb : FormBuilder) 
  {
    // validators.required, caracteristica para validar campos no vacios (requeridos)
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre : ['', Validators.compose(
        [Validators.required, this.nameValidator, this.nameValidatorParam(this.minLong)]
      )],
      url : [''],
      lugar : ['', Validators.required]
    });

    this.fg.valueChanges.subscribe((form : any) => {
      console.log('cambio el formulario: ', form)
    })
  }

  ngOnInit(): void {
  }
  
  guardar(nombre : string, url : string) : boolean
  {
    let d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  // validador perzonalizado
  // Metodo para validar campos del formulario, retornar un json de validaciones
  nameValidator(control : FormControl): {[s : string] : boolean;}
  {
    const l = control.value.toString().trim().length;
    if (l > 0 && l <5)
    {
      return {invalidName : true};
    }
    return null;
  }
  // valiadores parametrizables
  nameValidatorParam(minLong : number) : ValidatorFn 
  { return (control : FormControl): { [s : string] : boolean } | null => 
    {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong)
      {
        return {minLongName : true};
      }
      return null;
    }
  }
}
