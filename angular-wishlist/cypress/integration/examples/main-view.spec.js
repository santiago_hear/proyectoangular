describe('ventana principal', () => {
    it('idioma: spanish, encabezado correcto', () => {
        cy.visit('http://localhost:4200');
        cy.contains('angular-wishlist');
        cy.get('h1 b').should('contain', 'HOLAes');
    });
    it('cajas de texto de nombre', () => {
        cy.visit('http://localhost:4200');
        cy.get('form div div').should('contain', 'Nombre Requerido!');
    });
    it('cajas de texto de lugar', () => {
        cy.visit('http://localhost:4200');
        cy.get('form div div').should('contain', 'Lugar Requerido!');
    });
});