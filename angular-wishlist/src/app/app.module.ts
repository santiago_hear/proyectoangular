import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store} from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtools, StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClient, HttpClientModule, HttpHandler, HttpHeaders, HttpRequest } from '@angular/common/http';
import Dexie from 'node_modules/dexie';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
//import { DestinosApiClient } from './models/destinos-api-client.model';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { DestinosViajesState, DestinosViajesEffects, initializeDestinosViajesState, reducerDestinosViajes, InitMyDataAction } from './models/destinos-viajes-state.models';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { AuthService } from './services/auth.service';
import { UsuarioAutenticadoGuard } from './guards/usuario-autenticado/usuario-autenticado.guard';
import { VuelosDetalleComponent } from './components/vuelos/vuelos-detalle/vuelos-detalle.component';
import { VuelosMainComponent } from './components/vuelos/vuelos-main/vuelos-main.component';
import { VuelosComponent } from './components/vuelos/vuelos/vuelos.component';
import { VuelosMasInfoComponent } from './components/vuelos/vuelos-mas-info/vuelos-mas-info.component';
import { ReservasModule } from './reservas/reservas.module';
import { DestinoViaje } from './models/destino-viaje.model';
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

// app config
export interface AppConfig {
  apiEndpoint : string;
}
const APP_CONFIG_VALUE : AppConfig = {
  apiEndpoint : 'http://localhost:3000'
}
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
// fin app config 

// init routing
export const ChildenRoutesVuelos : Routes = [
  {path : '', redirectTo : 'main', pathMatch : 'full'},
  {path : 'main', component : VuelosMainComponent },
  {path : 'mas-info', component : VuelosMasInfoComponent },
  {path : ':id', component : VuelosDetalleComponent }
]

const routes : Routes = 
[
  {path : '', redirectTo : 'home', pathMatch : 'full'},
  {path : 'home', component : ListaDestinosComponent},
  {path : 'destino/:id', component : DestinoDetalleComponent},
  {path : 'login', component : LoginComponent},
  {path : 'protected', component : ProtectedComponent, canActivate : [ UsuarioAutenticadoGuard ]},
  {path : 'vuelos', component : VuelosComponent, canActivate : [ UsuarioAutenticadoGuard ], children : ChildenRoutesVuelos}
];
// fin routing 

//Redux init
export interface AppState{
  destinos : DestinosViajesState;
}

const reducers : ActionReducerMap<AppState> = {
  destinos : reducerDestinosViajes
};

const reducersInitialState  = {
  destinos : initializeDestinosViajesState()
};
// Fin redux init

// app init
export function init_app(appLoadService : AppLoadService) : () => Promise<any> {
  return () => appLoadService.initializeDestinosViajesState();
}

@Injectable()
class AppLoadService {
  constructor(private store : Store<AppState>, private http : HttpClient){ }
  async initializeDestinosViajesState() : Promise<any> {
    const headers : HttpHeaders = new HttpHeaders({'X-API-TOKEN' : 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', {headers : headers });
    const response : any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
    console.log('cargo las nuevas cards');
  }
}
// fin app init

// dexie db
@Injectable({
  providedIn : 'root'
})
// export class MyDatabase extends Dexie {
//   destinos : Dexie.Table<DestinoViaje, number>;
//   constructor () {
//     super('MyDatabase');
//     this.version(1).stores({
//       destinos : '++id, nombre, imagenUrl',
//     });
//   }
// }
export class Translation {
  constructor(public id : number, public lang : string, public key : string, public value : string){}
}

export class MyDatabase extends Dexie {
  destinos : Dexie.Table<DestinoViaje, number>;
  translations : Dexie.Table<Translation, number>;
  constructor () {
    super('MyDatabase');
    this.version(1).stores({
      destinos : '++id, nombre, imagenUrl'
    });
    this.version(2).stores({
      destinos : '++id, nombre, imagenUrl',
      translations : '++id, lang, key, value'
    });
  }
}

export const db = new MyDatabase();
// Fin dexie db

// i18n ini

class TranslationLoader implements TranslateLoader {
  constructor( private http : HttpClient) { }

  getTranslation(lang : string) : Observable<any> {
    const promise = db.translations
      .where('lang')
      .equals(lang)
      .toArray()
      .then(results => {
        if(results.length === 0){
          return this.http
            .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
            .toPromise()
            .then(apiResults => {
              db.translations.bulkAdd(apiResults);
              return apiResults;
            });
        }
        return results;
      }).then((traducciones) => {
        console.log('traducciones cargadas:');
        console.log(traducciones);
        return traducciones;
      }).then((traducciones) => {
        return traducciones.map((t) => ({ [t.key] : t.value}));
      });
  return from(promise).pipe(flatMap((elems) => from(elems)));
  }
}

function HttpLoaderFactory(http : HttpClient){
  return new TranslationLoader(http);
}

// fin i18n

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosDetalleComponent,
    VuelosMainComponent,
    VuelosComponent,
    VuelosMasInfoComponent,
    TrackearClickDirective,
    //EspiameDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, { 
      initialState : reducersInitialState,
      runtimeChecks : {
        strictStateImmutability : false,
        strictActionImmutability : false,
      } }),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader : {
        provide : TranslateLoader,
        useFactory : (HttpLoaderFactory),
        deps : [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [AuthService,
    UsuarioAutenticadoGuard,
    {provide : APP_CONFIG, useValue : APP_CONFIG_VALUE},
    AppLoadService, {provide : APP_INITIALIZER, useFactory : init_app, deps : [AppLoadService], multi : true},
    MyDatabase],
  bootstrap: [AppComponent]
})
export class AppModule { }
