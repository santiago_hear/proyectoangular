import { Directive, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';

@Directive({
  selector: '[appTrackearClick]'
})
export class TrackearClickDirective {

  private element : HTMLInputElement;

  constructor(private ref : ElementRef) {
    this.element = ref.nativeElement;
    fromEvent(this.element, 'click').subscribe(evento => this.track(evento));
   }

   track(evento : Event) : void {
     const tags = this.element.attributes.getNamedItem('data-trackear-tags').value.split(' ');
     console.log(`|||| track evento: "${tags}"`);
   }

}
