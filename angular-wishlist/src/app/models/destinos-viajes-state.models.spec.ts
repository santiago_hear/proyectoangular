import {
    reducerDestinosViajes,
    DestinosViajesState,
    initializeDestinosViajesState,
    InitMyDataAction,
    NuevoDestinoAction
} from './destinos-viajes-state.models';
import { DestinoViaje } from './destino-viaje.model';

describe('reducerDestinosViajes', () => {
    it('should reduce init data', () =>{
        // setup
        const prevState : DestinosViajesState = initializeDestinosViajesState();
        const action : InitMyDataAction = new InitMyDataAction(['barranquilla','manizales']);
        // action
        const newState : DestinosViajesState = reducerDestinosViajes(prevState, action);
        // assertions
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].name).toEqual('barranquilla');
        // tear down -> borrar lo que se inserto en la base de datos
    });

    it('should reduce new item added', () =>{
        const prevState : DestinosViajesState = initializeDestinosViajesState();
        const action : NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
        const newState : DestinosViajesState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].name).toEqual('barcelona');
    });
});