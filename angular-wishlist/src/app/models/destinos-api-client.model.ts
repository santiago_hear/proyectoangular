import { forwardRef, Inject, Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Subject } from 'rxjs';
import { AppConfig, AppState, APP_CONFIG, db, MyDatabase } from '../app.module';
import { DestinoViaje } from './destino-viaje.model';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-state.models';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinosApiClient
{
    destinos : DestinoViaje[] = [];
    //current : Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);

    constructor(
        private store : Store<AppState>, 
        @Inject(forwardRef(() => APP_CONFIG)) 
        private config : AppConfig, 
        private http : HttpClient){ 
        //this.destinos = [];
        this.store.select(state => state.destinos).subscribe((data) => {
            console.log('destinos sub store');
            console.log(data);
            this.destinos = data.items;
        });
        this.store.subscribe((data) => {
            console.log('all store');
            console.log(data);
        });
    }

    // add(d : DestinoViaje)
    // {
    //     this.store.dispatch(new NuevoDestinoAction(d));
    //     //this.destinos.push(d);
    // }

    add(d  : DestinoViaje)
    {
        const headers : HttpHeaders = new HttpHeaders({'X-API-TOKEN' : 'token-seguridad'});
        const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', {nuevo : d.name }, {headers : headers});
        this.http.request(req).subscribe((data : HttpResponse<{}>) => {
            if(data.status === 200)
            {
                this.store.dispatch(new NuevoDestinoAction(d));
                const myDb = db;
                myDb.destinos.add(d);
                console.log('todos los destinos a la db');
                myDb.destinos.toArray().then(destinos => console.log(destinos));
            }
        });
    }

    getAll() : DestinoViaje[]
    {
        return this.destinos;
    }

    getById(id : String) : DestinoViaje
    {
        return this.destinos.filter((d) => { return d.id.toString() == id; })[0];
    }

    // Para marcar destinos elegidos en toda la aplicacion
    elegir(d : DestinoViaje)
    {
        this.store.dispatch(new ElegidoFavoritoAction(d));
        // this.destinos.forEach(x => x.setSeleted(false));
        // d.setSeleted(true);
        // this.current.next(d);
    }

    // Para que los demas se puedan suscribir
    // suscribeOnChange(fn)
    // {
    //     this.current.subscribe(fn);
    // }
}