import {v4 as uuid} from 'uuid';

export class DestinoViaje{
    private selected : boolean;
    public servicios : string [];
    id = uuid();

    constructor(public name: string, public ulr: string, public votes : number = 0){
        this.servicios = ["Buffet", "Piscina"];
    }

    isSelected(): boolean
    {
        return this.selected;
    }

    setSeleted(sel : boolean)
    {
        this.selected = sel;
    } 

    voteUp()
    {
        this.votes ++;
    }

    voteDown()
    {
        this.votes --;
    }
}