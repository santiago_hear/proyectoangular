import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { DestinoViaje } from './../../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers : [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit 
{
  @Output() onItemAdded : EventEmitter<DestinoViaje>;
  updates : string[];
  all;

  constructor(public destinosApiClient : DestinosApiClient, private store : Store<AppState>) 
  {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito).subscribe(d =>
      {
        if (d != null)
        {
          this.updates.push('se ha elegido a: ' + d.name);
        }
      });
      this.all = store.select(state => state.destinos.items).subscribe(items => this.all = items);
    // this.destinosApiClient.suscribeOnChange((d: DestinoViaje) => {
    //   if (d != null)
    //   {
    //     this.updates.push('se ha elegido a: ' + d.name);
    //   }
    // })
  }

  ngOnInit(): void {
  }

  agregado(d : DestinoViaje)
  {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    //this.store.dispatch(new NuevoDestinoAction(d));
  }
  
  /*
  guardar(nombre:string, url:string):boolean
  {
    this.destinos.push(new DestinoViaje(nombre,url));
    console.log(this.destinos);
    return false;  
  }
  */

  /*
  elegido(d: DestinoViaje)
  {
    this.destinos.forEach(function(x){ x.setSeleted(false)})
    d.setSeleted(true);
  }
  */
  elegido(e: DestinoViaje)
  {
    this.destinosApiClient.elegir(e);
    //this.store.dispatch(new ElegidoFavoritoAction(e));
  }
  
  /*
 elegido(e: DestinoViaje)
 {
   this.destinosApiClient.getAll().forEach(function(x){ x.setSeleted(false)})
   e.setSeleted(true);
 }
 */

}
